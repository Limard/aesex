package AesEx

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
)

func cmReaderCBCEncrypt(data, key string) (string, error) {
	iv := `abcdefghijklmnop`

	keyByte, _ := base64.StdEncoding.DecodeString(key)
	encoder := AesCryptorCBC{}
	b, e := encoder.encrypt([]byte(data), keyByte, []byte(iv))
	if e != nil {
		return "", e
	}
	return string(b), nil
}

func cmReaderCBCDecrypt(data, key string) (string, error) {
	return "", nil
}

type AesCryptorCBC struct{}

func (t *AesCryptorCBC) encrypt(data, key, iv []byte) ([]byte, error) {
	aesBlockEncrypter, err := aes.NewCipher(key)
	if err != nil {
		println(err.Error())
		return nil, err
	}

	data = PKCS5Padding(data, aesBlockEncrypter.BlockSize())
	aesEncrypter := cipher.NewCBCEncrypter(aesBlockEncrypter, iv)
	encrypted := make([]byte, len(data))
	aesEncrypter.CryptBlocks(encrypted, data)
	return encrypted, nil
}

func (t *AesCryptorCBC) decrypt(src, key, iv []byte) (data []byte, err error) {
	aesBlockDecrypter, err := aes.NewCipher(key)
	if err != nil {
		println(err.Error())
		return nil, err
	}

	decrypted := make([]byte, len(src))
	aesDecrypter := cipher.NewCBCDecrypter(aesBlockDecrypter, iv)
	aesDecrypter.CryptBlocks(decrypted, src)
	return PKCS5UnPadding(decrypted), nil
}
