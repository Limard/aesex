AES五种加密模式

1.电码本模式（Electronic Codebook Book (ECB)）
    将整个明文分成若干段相同的小段，然后对每一小段进行加密。

2.密码分组链接模式（Cipher Block Chaining (CBC)）
    先将明文切分成若干小段，然后每一小段与初始块或者上一段的密文段进行异或运算后，再与密钥进行加密。

3.计算器模式（Counter (CTR)）
    不常见

4.密码反馈模式（Cipher FeedBack (CFB)）
    较复杂

5.输出反馈模式（Output FeedBack (OFB)）
    较复杂


refer: https://www.cnblogs.com/starwolf/p/3365834.html